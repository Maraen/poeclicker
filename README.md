# How to install PoEClicker

### Without an android phone :

1. Download the git file.
2. (Download Android Studio)
   <a href="https://developer.android.com/studio" target="_blank">click here for download</a>
3. Open it Android Studio
4. Use an emulator that has a google play store
   (needed for firebase)
5. Run on emulator or on an Android device

### With an android phone:

Download the apk package named `myApp.apk `
