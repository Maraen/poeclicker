package com.leo.poeclicker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.leo.poeclicker.Models.SharedViewModel;
import com.leo.poeclicker.Models.User;
import com.leo.poeclicker.Upgrades.ExtraClicks;
import com.leo.poeclicker.Views.ClickableCurrency;
import com.leo.poeclicker.Views.LeaderboardDialog;
import com.leo.poeclicker.Views.LoginDialog;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements LoginDialog.UsernameDialogListener{

//    SharedViewModel model;
//    Declare database
    private DatabaseReference mDatabase;



    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String USERNAME = "username";
    public static final String WEALTH = "wealth";
    public static final String EXTRA_CLICKS_COUNTER = "extra_clicks_counter";
    public static final String COST_OF_EXTRACLICKS = "cost_of_extraclicks";
    public static final String CURRENCY_TIME_UPGRADE_1 = "currency_time_upgrade_1";
    public static final String COST_OF_COT1 = "cost_of_cot1";
    public static final String CURRENCY_TIME_UPGRADE_2 = "currency_time_upgrade_2";
    public static final String COST_OF_COT2 = "cost_of_cot2";
    public static final String CURRENCY_TIME_UPGRADE_3 = "currency_time_upgrade_3";
    public static final String COST_OF_COT3 = "cost_of_cot3";
    public static final String CURRENCY_TIME_UPGRADE_4 = "currency_time_upgrade_4";
    public static final String COST_OF_COT4 = "cost_of_cot4";
    public static final String ID = "id";
    public static final String LAST_LOGGED_TIMESTAMP = "last_logged_timestamp";
    public long current_tmp = System.currentTimeMillis() / 1000;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final SharedViewModel model =  new ViewModelProvider(this).get(SharedViewModel.class);


        final TextView currentWealthCounter = findViewById(R.id.CurrentWealth);

//        loading from localstorage.
        final SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();

        model.intCurrentWealth = Integer.parseInt(sharedPreferences.getString(WEALTH, "0"));
//        TODO
//        TODO
//        TODO CHANGE MY LINE BELOW: TO DEFVALUE 1 / IT SHOWS ONLY 1 ON
//        TODO THE FIRST TEXT BUT THAT IS WORKING IS INTENDED SINCE IT'S NOT DYNAMIC
        model.extraClicks = sharedPreferences.getInt(EXTRA_CLICKS_COUNTER, 1);
        model.stateOfUpgradeCoT1 = sharedPreferences.getInt(CURRENCY_TIME_UPGRADE_1, 0);
        model.stateOfUpgradeCoT2 = sharedPreferences.getInt(CURRENCY_TIME_UPGRADE_2, 0);
        model.stateOfUpgradeCoT3 = sharedPreferences.getInt(CURRENCY_TIME_UPGRADE_3, 0);
        model.stateOfUpgradeCoT4 = sharedPreferences.getInt(CURRENCY_TIME_UPGRADE_4, 0);
        model.stackedCoTUpgrades = model.stateOfUpgradeCoT1 + model.stateOfUpgradeCoT2 +
                model.stateOfUpgradeCoT3 + model.stateOfUpgradeCoT4;

//        Calculation when logging back on, delta of currency made.
        long past_logged_tmp = sharedPreferences.getLong(LAST_LOGGED_TIMESTAMP, 0);
        long delta_time = current_tmp - past_logged_tmp;
        long delta_wealth = delta_time * model.stackedCoTUpgrades / 5;
        model.intCurrentWealth = model.intCurrentWealth + (int)delta_wealth;
        model.getCurrentWealth().setValue(Integer.toString(model.intCurrentWealth));


//        currentWealth Observer. Updates and saves most parameter on change.
        final Observer<String> wealthObserver = new Observer<String>() {
            @Override
            public void onChanged(String string) {
                currentWealthCounter.setText(string);


//                this editor.put and editor.apply are asynchronus and very low cpu/memory intensif.
                editor.putString(WEALTH, Integer.toString(model.intCurrentWealth));
                editor.putInt(EXTRA_CLICKS_COUNTER, model.extraClicks);
                editor.putInt(COST_OF_EXTRACLICKS,model.costOfExtraclicks);
                editor.putInt(CURRENCY_TIME_UPGRADE_1, model.stateOfUpgradeCoT1);
                editor.putInt(COST_OF_COT1, model.costOfCoT1);
                editor.putInt(CURRENCY_TIME_UPGRADE_2, model.stateOfUpgradeCoT2);
                editor.putInt(COST_OF_COT2, model.costOfCoT2);
                editor.putInt(CURRENCY_TIME_UPGRADE_3, model.stateOfUpgradeCoT3);
                editor.putInt(COST_OF_COT3, model.costOfCoT3);
                editor.putInt(CURRENCY_TIME_UPGRADE_4, model.stateOfUpgradeCoT4);
                editor.putInt(COST_OF_COT4, model.costOfCoT4);
                editor.putLong(LAST_LOGGED_TIMESTAMP, current_tmp);
                editor.apply();

            }
        };

        model.getCurrentWealth().observe(this, wealthObserver);

//        Saving username to localStorage
        String loggedUsername = sharedPreferences.getString(USERNAME, "");
        if(loggedUsername != ""){
            Toast toast = Toast.makeText(getApplicationContext(), "Welcome back " + loggedUsername, Toast.LENGTH_LONG);
            toast.show();
        } else {
            openDialog();
        }





    }

    public void openDialog(){
        LoginDialog loginDialog = new LoginDialog();
        loginDialog.show(getSupportFragmentManager(), "login dialog");
    }

    public void openLeaderboard(){
        LeaderboardDialog leaderboardDialog = new LeaderboardDialog();
        leaderboardDialog.show(getSupportFragmentManager(), "leaderboard dialog");
    }

    @Override
    public void applyUsername(String username) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(USERNAME, username);
        editor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();

        final SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();

        //        writing to database to add
        Button buttontest = findViewById(R.id.buttonTesting);
        mDatabase = FirebaseDatabase.getInstance().getReference("users");
        TimerTask updateFirebase = new TimerTask() {
            @Override
            public void run() {
                if (sharedPreferences.getString(ID, "").equals("")) {
                    String id = mDatabase.push().getKey();
                    editor.putString(ID, id);
                    editor.apply();
                }

                String id = sharedPreferences.getString(ID, "");

                if(mDatabase.child(id).equals(id)){

                } else {
                    final String name = sharedPreferences.getString(USERNAME, "");
                    final String wealth = sharedPreferences.getString(WEALTH, "");
                    User user = new User(name, wealth);
                    mDatabase.child(id).setValue(user);
                }
                mDatabase.child(id).child("wealth").setValue(sharedPreferences.getString(WEALTH,""));
            }
        };


        Timer timer = new Timer();
        timer.scheduleAtFixedRate(updateFirebase, 0 , 10000);

//          Open Leaderboard dialog
        buttontest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mDatabase.child(sharedPreferences.getString(ID, "")).child("username").equals("")){
                    mDatabase.child(sharedPreferences.getString(ID, "")).child("username")
                            .setValue(sharedPreferences.getString(USERNAME, ""));
                }
                openLeaderboard();
            }
        });


    }
}
