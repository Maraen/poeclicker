package com.leo.poeclicker.Models;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel {
    private MutableLiveData<String> currentWealth;

    public MutableLiveData<String> getCurrentWealth(){
        if(currentWealth == null){
            currentWealth = new MutableLiveData<>();
        }
        return currentWealth;
    }


    public int extraClicks = 1;
    public int intCurrentWealth = 0;
    public int stateOfUpgradeCoT1 = 0;
    public int stateOfUpgradeCoT2 = 0;
    public int stateOfUpgradeCoT3 = 0;
    public int stateOfUpgradeCoT4 = 0;
    public int stackedCoTUpgrades = stateOfUpgradeCoT1 + stateOfUpgradeCoT2 + stateOfUpgradeCoT3 + stateOfUpgradeCoT4;
    public int costOfExtraclicks = 10;
    public int costOfCoT1 = 200;
    public int costOfCoT2 = 20000;
    public int costOfCoT3 = 500000;
    public int costOfCoT4 = 1000000;
}
