package com.leo.poeclicker.Upgrades;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.leo.poeclicker.Models.SharedViewModel;
import com.leo.poeclicker.R;


public class CurrencyOverTime1 extends Fragment {

    private SharedViewModel model;
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String COST_OF_COT1 = "cost_of_cot1";
    public static final String CURRENCY_TIME_UPGRADE_1 = "currency_time_upgrade_1";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.overtime_upgrade_1, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        model = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);

        final Button overtimeUpgrade1 = view.findViewById(R.id.ButtonCoT1);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        int loggedCost = sharedPreferences.getInt(COST_OF_COT1, 200);

        if(model.costOfCoT1 < loggedCost){
            model.costOfCoT1 = loggedCost;
            model.stateOfUpgradeCoT1 = sharedPreferences.getInt(CURRENCY_TIME_UPGRADE_1, 0);
            overtimeUpgrade1.setText("Gaining " + model.stateOfUpgradeCoT1 +" Currency per 5 sec\ncost: " + model.costOfCoT1);
        }


//        Upgrading method
        overtimeUpgrade1.setOnClickListener(new View.OnClickListener() {
            private double costOfUpgrade = model.costOfCoT1;
            @Override
            public void onClick(View v) {
//                  Check for cost
                if(model.intCurrentWealth >= model.costOfCoT1) {

//                    Applying the upgrade and applying the cost
                    model.stateOfUpgradeCoT1 = model.stateOfUpgradeCoT1 + 1;
                    model.intCurrentWealth = model.intCurrentWealth - model.costOfCoT1;
                    model.getCurrentWealth().setValue(Integer.toString(model.intCurrentWealth));

//                    increasing the cost, this is the reason for using 2 values
                    costOfUpgrade = costOfUpgrade * 1.4;
                    model.costOfCoT1 = (int)costOfUpgrade;

//                    Original Message "Gain 1 Currency per 5 sec cost : 200" - setting message only in it's fragment
                    overtimeUpgrade1.setText("Gaining " + model.stateOfUpgradeCoT1 +" Currency per 5 sec\ncost: " + model.costOfCoT1);
                }
            }
        });
    }
}
