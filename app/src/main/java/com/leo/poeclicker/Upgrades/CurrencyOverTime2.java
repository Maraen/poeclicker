package com.leo.poeclicker.Upgrades;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.leo.poeclicker.Models.SharedViewModel;
import com.leo.poeclicker.R;


public class CurrencyOverTime2 extends Fragment {

    private SharedViewModel model;
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String COST_OF_COT2 = "cost_of_cot2";
    public static final String CURRENCY_TIME_UPGRADE_2 = "currency_time_upgrade_2";



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.overtime_upgrade_2, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        model = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);

        final Button overtimeUpgrade2 = view.findViewById(R.id.ButtonCoT2);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        int loggedCost = sharedPreferences.getInt(COST_OF_COT2, 20000);

        if(model.costOfCoT2 < loggedCost){
            model.costOfCoT2 = loggedCost;
            model.stateOfUpgradeCoT2 = sharedPreferences.getInt(CURRENCY_TIME_UPGRADE_2, 0);
            overtimeUpgrade2.setText("Gaining " + model.stateOfUpgradeCoT2 +" Currency per 5 sec\ncost: " + model.costOfCoT2);
        }


//        Upgrading method
        overtimeUpgrade2.setOnClickListener(new View.OnClickListener() {
            private double costOfUpgrade = model.costOfCoT2;
            @Override
            public void onClick(View v) {
//                  Check for cost
                if(model.intCurrentWealth >= model.costOfCoT2) {

//                    Applying the upgrade and applying the cost
                    model.stateOfUpgradeCoT2 = model.stateOfUpgradeCoT2 + 10;
                    model.intCurrentWealth = model.intCurrentWealth - model.costOfCoT2;
                    model.getCurrentWealth().setValue(Integer.toString(model.intCurrentWealth));

//                    increasing the cost, this is the reason for using 2 values
                    costOfUpgrade = costOfUpgrade * 1.4;
                    model.costOfCoT2 = (int)costOfUpgrade;

//                    Original Message "Gain 1 Currency per 5 sec cost : 200" - setting message only in it's fragment
                    overtimeUpgrade2.setText("Gaining " + model.stateOfUpgradeCoT2 +" Currency per 5 sec\ncost: " + model.costOfCoT2);
                }
            }
        });
    }
}
