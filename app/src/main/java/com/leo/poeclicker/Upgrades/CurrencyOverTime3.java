package com.leo.poeclicker.Upgrades;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.leo.poeclicker.Models.SharedViewModel;
import com.leo.poeclicker.R;


public class CurrencyOverTime3 extends Fragment {

    private SharedViewModel model;
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String COST_OF_COT3 = "cost_of_cot3";
    public static final String CURRENCY_TIME_UPGRADE_3 = "currency_time_upgrade_3";



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.overtime_upgrade_3, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        model = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);

        final Button overtimeUpgrade3 = view.findViewById(R.id.ButtonCoT3);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        int loggedCost = sharedPreferences.getInt(COST_OF_COT3, 500000);

        if(model.costOfCoT3 < loggedCost){
            model.costOfCoT3 = loggedCost;
            model.stateOfUpgradeCoT3 = sharedPreferences.getInt(CURRENCY_TIME_UPGRADE_3, 0);
            overtimeUpgrade3.setText("Gaining " + model.stateOfUpgradeCoT3 +" Currency per 5 sec\ncost: " + model.costOfCoT3);
        }


//        Upgrading method
        overtimeUpgrade3.setOnClickListener(new View.OnClickListener() {
            private double costOfUpgrade = model.costOfCoT3;
            @Override
            public void onClick(View v) {
//                  Check for cost
                if(model.intCurrentWealth >= model.costOfCoT3) {

//                    Applying the upgrade and applying the cost
                    model.stateOfUpgradeCoT3 = model.stateOfUpgradeCoT3 + 100;
                    model.intCurrentWealth = model.intCurrentWealth - model.costOfCoT3;
                    model.getCurrentWealth().setValue(Integer.toString(model.intCurrentWealth));

//                    increasing the cost, this is the reason for using 2 values
                    costOfUpgrade = costOfUpgrade * 1.4;
                    model.costOfCoT3 = (int)costOfUpgrade;

//                    Original Message "Gain 1 Currency per 5 sec cost : 200" - setting message only in it's fragment
                    overtimeUpgrade3.setText("Gaining " + model.stateOfUpgradeCoT3 +" Currency per 5 sec\ncost: " + model.costOfCoT3);

                }



            }
        });
    }
}
