package com.leo.poeclicker.Upgrades;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.leo.poeclicker.Models.SharedViewModel;
import com.leo.poeclicker.R;


public class CurrencyOverTime4 extends Fragment {

    private SharedViewModel model;
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String COST_OF_COT4 = "cost_of_cot4";
    public static final String CURRENCY_TIME_UPGRADE_4 = "currency_time_upgrade_4";



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.overtime_upgrade_4, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        model = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);

        final Button overtimeUpgrade4 = view.findViewById(R.id.ButtonCoT4);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        int loggedCost = sharedPreferences.getInt(COST_OF_COT4, 10000000);

        if(model.costOfCoT4 < loggedCost){
            model.costOfCoT4 = loggedCost;
            model.stateOfUpgradeCoT4 = sharedPreferences.getInt(CURRENCY_TIME_UPGRADE_4, 0);
            overtimeUpgrade4.setText("Gaining " + model.stateOfUpgradeCoT4 +" Currency per 5 sec\ncost: " + model.costOfCoT4);
        }


//        Upgrading method
        overtimeUpgrade4.setOnClickListener(new View.OnClickListener() {
            private double costOfUpgrade = model.costOfCoT4;
            @Override
            public void onClick(View v) {

//                  Check for cost
                if(model.intCurrentWealth >= model.costOfCoT4) {

//                    Applying the upgrade and applying the cost
                    model.stateOfUpgradeCoT4 = model.stateOfUpgradeCoT4 + 10000;
                    model.intCurrentWealth = model.intCurrentWealth - model.costOfCoT4;
                    model.getCurrentWealth().setValue(Integer.toString(model.intCurrentWealth));

//                    increasing the cost, this is the reason for using 2 values
                    costOfUpgrade = costOfUpgrade * 1.6;
                    model.costOfCoT4 = (int)costOfUpgrade;

//                    Original Message "Gain 1 Currency per 5 sec cost : 200" - setting message only in it's fragment
                    overtimeUpgrade4.setText("Gaining " + model.stateOfUpgradeCoT4 +" Currency per 5 sec\ncost: " + model.costOfCoT4);

                }



            }
        });
    }
}
