package com.leo.poeclicker.Upgrades;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.leo.poeclicker.Models.SharedViewModel;
import com.leo.poeclicker.R;

import java.util.Timer;
import java.util.TimerTask;

public class CurrencyOverTimeManager extends Fragment {

    private SharedViewModel model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.upgrade_container, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        model = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);

        final Runnable updateCurrencyOverTime = new Runnable() {
            @Override
            public void run() {
                model.stackedCoTUpgrades = model.stateOfUpgradeCoT1 + model.stateOfUpgradeCoT2
                + model.stateOfUpgradeCoT3 + model.stateOfUpgradeCoT4;
                model.intCurrentWealth = model.intCurrentWealth + model.stackedCoTUpgrades;
                model.getCurrentWealth().setValue(Integer.toString(model.intCurrentWealth));
            }
        };
//        COT = CurrentOverTime, since the timer works on the UI it needs the Activity.runOnUiThread to work.
        final TimerTask taskUpdateCOT = new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(updateCurrencyOverTime);
            }
        };

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(taskUpdateCOT,0 ,5000);
    }
}
