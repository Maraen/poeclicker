package com.leo.poeclicker.Upgrades;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.leo.poeclicker.Models.SharedViewModel;
import com.leo.poeclicker.R;

public class ExtraClicks extends Fragment {

    private SharedViewModel model;
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String COST_OF_EXTRACLICKS = "cost_of_extraclicks";



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.extra_click_upgrade, container, false);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        model = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);

        final Button extraClicker = view.findViewById(R.id.ExtraClickButton);

//        checking if there was previously an increased cost to the upgrade.
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        int loggedCost = sharedPreferences.getInt(COST_OF_EXTRACLICKS, 10);

        if(model.costOfExtraclicks < loggedCost){
            model.costOfExtraclicks = loggedCost;
            extraClicker.setText("Upgrade your clicks by : "+ sharedPreferences.getInt("extra_clicks_counter", 1) +"\ncost:" + model.costOfExtraclicks);
        }

        extraClicker.setOnClickListener(new View.OnClickListener() {
            double costOfUpgrade = model.costOfExtraclicks;
            @Override
            public void onClick(View v) {
                int extraclicks = model.extraClicks;

//                  Upgrade the number of extraHands
                if(model.intCurrentWealth >= model.costOfExtraclicks) {
                    extraclicks = extraclicks + 1;
                    model.extraClicks = extraclicks;
                    model.intCurrentWealth = model.intCurrentWealth - model.costOfExtraclicks;
                    model.getCurrentWealth().setValue(Integer.toString(model.intCurrentWealth));
//                    Need to do calculation on a (double) value and then convert to int for better formatting / calculation
                    costOfUpgrade = costOfUpgrade * 1.4;
                    model.costOfExtraclicks = (int)costOfUpgrade;
//                    Update Button text to reflect upgrade cost.
                    extraClicker.setText("Upgrade your clicks by : "+ model.extraClicks
                            +"\ncost : " + model.costOfExtraclicks);

                }
                else{
                    Log.d("PointsError", "Not enough points");
                }




            }
        });
    }
}
