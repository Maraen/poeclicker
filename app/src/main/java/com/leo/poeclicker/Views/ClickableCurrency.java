package com.leo.poeclicker.Views;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.leo.poeclicker.Models.SharedViewModel;
import com.leo.poeclicker.R;
import com.leo.poeclicker.Upgrades.ExtraClicks;

import java.util.Timer;
import java.util.TimerTask;

// this class permits to update the value of currency on click(and also power up clicks), but it does not update the UI.

public class ClickableCurrency extends Fragment{
//    TODO clean up commentaries
//    Thanks to this callback we can now link the Fragment and the activity
//    int currentWealth = 0;
    private SharedViewModel model;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.button_fragment, container, false);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        model = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);

//        My Main Clicking button
        final Button mainClicker = view.findViewById(R.id.ButtonCurrencyClicker);

//         Do Code for onClick here.
        mainClicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                model.intCurrentWealth = model.intCurrentWealth + model.extraClicks;

//                TODO is this runOnUiThread even useful ? I'm not affecting the UI

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        model.getCurrentWealth().setValue(Integer.toString(model.intCurrentWealth));
                    }
                });

//                Visual updates onto the Chaos button, Just a grow on click

                mainClicker.setLayoutParams(new LinearLayout.LayoutParams(300, 300));


                TimerTask growOnClick = new TimerTask() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mainClicker.setLayoutParams(new LinearLayout.LayoutParams(400,400));
                            }
                        });
                    }
                };

                Timer timer = new Timer();
                timer.schedule(growOnClick, 100);






            }
        });
    }
}
