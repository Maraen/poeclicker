package com.leo.poeclicker.Views;

import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.leo.poeclicker.MainActivity;
import com.leo.poeclicker.Models.User;
import com.leo.poeclicker.R;
import com.leo.poeclicker.Utils.RecyclerAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class LeaderboardDialog extends AppCompatDialogFragment {

//    Widgets
    RecyclerView recyclerView;
//    Firebase
    private DatabaseReference mDatabase;
//    Variables
    private ArrayList<User> usersList;
    private RecyclerAdapter recyclerAdapter;
    private Context mContext;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        TODO inflate view, cf: LoginDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.leaderboard_dialog, null);

        builder.setView(view)
                .setTitle("Leaderboard")
                .setPositiveButton("close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        recyclerView = view.findViewById(R.id.recyclerViewLeaderboard);

//        codehere
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

//        Firebase
        mDatabase = FirebaseDatabase.getInstance().getReference();

//        Arraylist:
        usersList = new ArrayList<>();
        ClearAll();

//        Get Data Method
        GetDataFromFirebase();

        return builder.create();
    }

    private void GetDataFromFirebase(){
        Query query = mDatabase.child("users");

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot : snapshot.getChildren()){
                    User users = new User();

                    users.setUsername(dataSnapshot.child("username").getValue().toString());
                    users.setWealth(dataSnapshot.child("wealth").getValue().toString());

                    usersList.add(users);

                }

                Collections.sort(usersList, new Comparator<User>() {
                    @Override
                    public int compare(User lhs, User rhs){
                        return Integer.compare(Integer.parseInt(rhs.getWealth()),Integer.parseInt(lhs.getWealth()));


                    }
                });

                recyclerAdapter = new RecyclerAdapter(getActivity().getApplicationContext(),usersList);
                recyclerView.setAdapter(recyclerAdapter);
                recyclerAdapter.notifyDataSetChanged();



//                Sort List in descending order from wealth

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void ClearAll(){
        if(usersList != null){
            usersList.clear();

            if(recyclerAdapter != null){
                recyclerAdapter.notifyDataSetChanged();
            }
        }
            usersList = new ArrayList<>();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);


    }
}
